package com.example.tugas3;

import androidx.appcompat.app.AppCompatActivity;

import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    private ImageView imageViewHair;
    private ImageView imageViewEyebrows;
    private ImageView  imageViewBeard;
    private ImageView imageViewMustache;
    private CheckBox checkBoxHair;
    private CheckBox checkBoxMustache;
    private CheckBox checkBoxBeard;
    private CheckBox checkBoxEyebrows;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setImageViewObject();
        setCheckboxObject();
        listenCheckBoxChange();
    }
    private void listenCheckBoxChange(){
        ImageView imageViewHair = this.imageViewHair;
        ImageView imageViewEyebrows = this.imageViewEyebrows;
        ImageView imageViewBeard = this.imageViewBeard;
        ImageView imageViewMustache = this.imageViewMustache;
        this.checkBoxMustache.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(compoundButton.isChecked()){
                    imageViewMustache.setVisibility(View.VISIBLE);
                }else{
                    imageViewMustache.setVisibility(View.INVISIBLE);
                }
            }
        });
        this.checkBoxBeard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(compoundButton.isChecked()){
                    imageViewBeard.setVisibility(View.VISIBLE);
                }else{
                    imageViewBeard.setVisibility(View.INVISIBLE);
                }
            }
        });
        this.checkBoxEyebrows.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(compoundButton.isChecked()){
                    imageViewEyebrows.setVisibility(View.VISIBLE);
                }else{
                    imageViewEyebrows.setVisibility(View.INVISIBLE);
                }
            }
        });
        this.checkBoxHair.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(compoundButton.isChecked()){
                    imageViewHair.setVisibility(View.VISIBLE);
                }else{
                    imageViewHair.setVisibility(View.INVISIBLE);
                }
            }
        });
    }
    private void setImageViewObject(){
        this.imageViewBeard = findViewById(R.id.imageViewBeard);
        this.imageViewHair = findViewById(R.id.imageViewHair);
        this.imageViewEyebrows = findViewById(R.id.imageViewEyebrows);
        this.imageViewMustache = findViewById(R.id.imageViewMustache);
    }
    private void setCheckboxObject(){
        this.checkBoxHair = findViewById(R.id.checkBoxHair);
        this.checkBoxMustache = findViewById(R.id.checkBoxMustache);
        this.checkBoxBeard = findViewById(R.id.checkBoxBeard);
        this.checkBoxEyebrows = findViewById(R.id.checkBoxEyebrows);
        this.checkBoxEyebrows.setChecked(true);
        this.checkBoxHair.setChecked(true);
        this.checkBoxMustache.setChecked(true);
        this.checkBoxBeard.setChecked(true);
    }
}